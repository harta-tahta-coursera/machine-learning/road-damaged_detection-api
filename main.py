import os
import cv2
import numpy as np
import tensorflow as tf
import model
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from object_detection.utils import visualization_utils as viz_utils
from starlette.exceptions import HTTPException as StarletteHTTPException
from google.cloud import storage
from datetime import datetime

client = storage.Client()
bucket_name = os.environ.get('BUCKET_NAME')
bucket = client.get_bucket(bucket_name)
detection_model, category_index = model.load()

app = FastAPI()


label = {
    1: 'D00',
    2: 'D01',
    3: 'D10',
    4: 'D11',
    5: 'D20',
    6: 'D40',
    7: 'D43',
    8: 'D44',
    9: 'D50'
}


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse(
        status_code=exc.status_code,
        content=jsonable_encoder({
            "status": exc.status_code,
            "message": "ERROR",
            "data": exc.detail
        })
    )

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return JSONResponse(
        status_code=400,
        content=jsonable_encoder({
            "status": 400,
            "message": "Invalid",
            "data": str(exc)
        })
    )

@app.post('/predict')
async def predict(image: UploadFile = File(...)):
    uploaded_image = await image.read()
    image_arr = np.fromstring(uploaded_image, np.uint8)
    img = cv2.imdecode(image_arr, cv2.IMREAD_UNCHANGED)
    image_np = np.array(img)
    input_tensor = tf.convert_to_tensor(
        np.expand_dims(image_np, 0), dtype=tf.float32)

    tf_image, shapes = detection_model.preprocess(input_tensor)
    prediction_dict = detection_model.predict(tf_image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)

    num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                  for key, value in detections.items()}
    detections['num_detections'] = num_detections

    # detection_classes should be ints.
    detections['detection_classes'] = detections['detection_classes'].astype(
        np.int64)

    label_id_offset = 1
    image_np_with_detections = image_np.copy()
    classes = detections['detection_classes']+label_id_offset
    scores = detections['detection_scores']

    viz_utils.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections,
        detections['detection_boxes'],
        classes,
        scores,
        category_index,
        use_normalized_coordinates=True,
        max_boxes_to_draw=3,
        min_score_thresh=.4,
        agnostic_mode=False)

    now = datetime.now()
    current_time = now.strftime("%Y-%m-%d_%H:%M:%S")
    filename = current_time + '_' + image.filename
    cv2.imwrite(filename, image_np_with_detections)
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)
    os.remove(filename)

    found = {}
    for cls in classes[:3]:
        found[label[cls.item()]] = True

    return {
        "succees": 200,
        "message": "OK",
        "data": {
            "imageUrl": blob.public_url,
            "classes": list(found.keys()),
            "score": scores[0].item()
        }
    }